﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveScreenFader : MonoBehaviour {

	// Use this for initialization


	void Start()
	{
		StartCoroutine(RemoveFader());
	}

	IEnumerator RemoveFader()
	{
		yield return new WaitForSeconds(2);
		Destroy (GameObject.Find ("Image"));
		Debug.Log ("Fader Removed Successfully");

	}
	// Update is called once per frame
	void Update () {

	}
}


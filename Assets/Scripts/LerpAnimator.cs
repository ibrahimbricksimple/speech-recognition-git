﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LerpAnimator : MonoBehaviour {

	public GameObject startMarker;
	public GameObject endMarker;
	public float speed = 1.0F;
	private float startTime;
	private float journeyLength;

	void Start() {
		startTime = Time.time;
		journeyLength = Vector3.Distance(startMarker.transform.position, endMarker.transform.position);
	}

	public void LerpAnimate(GameObject begin, GameObject end){
		//Debug.Log ("Initiate animation");

		startMarker = begin;
		endMarker = end;
		startTime = Time.time;
		journeyLength = Vector3.Distance(startMarker.transform.position, endMarker.transform.position);
	}

	void Update() { 
		float distCovered = (Time.time - startTime) * speed;
		float fracJourney = distCovered / journeyLength;

		//ensure that fracJourney has a valid value before assigning
		if (!float.IsNaN (fracJourney)) {
			transform.position = Vector3.Lerp (startMarker.transform.position, endMarker.transform.position, fracJourney);
		}else {
			//Debug.Log ("Error: Invalid fracJourney");
		}
	}
}

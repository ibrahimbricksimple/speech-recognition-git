using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Oculus;
using UnityEngine.SceneManagement;
public class UserDetection : MonoBehaviour
{
	bool splashLoaded, isPaused;
	public GameObject video;
	// Use this for initialization
	void Start()
	{
		splashLoaded = true;
	}

	// Update is called once per frame
	void Update()
	{
		isPaused = OVRPlugin.userPresent;
		Detect();
	}
	void Detect()
	{
		if (isPaused && !splashLoaded)
		{
			SceneManager.LoadScene("Intro");
		}
		if (!isPaused)
		{
			/*
			if (SceneManager.GetActiveScene().name == "Video")
			{
				video.GetComponent<MediaPlayerCtrl>().SetVolume(0);
			}
			*/
			splashLoaded = false;
		}
	}
	void OnApplicationPause(bool pauseStatus)
	{
		isPaused = pauseStatus;
	}
}
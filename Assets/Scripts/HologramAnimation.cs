﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class HologramAnimation : MonoBehaviour {

	private Animator calendarPatients;
	private Animator newsTreatments;
	private Animator calendar;
	private Animator calendarHighlight;
	private Animator news;
	private Animator patientList;
	private Animator productList;
	private Animator patientProfile;
	private Animator productProfile;
	public static string activeAnimation;
	private static bool calendarOpen = false;
	private static bool newsOpen = false;
	private float currentLocation;
	private LerpAnimator CalendarLerp;
	private LerpAnimator NewsLerp;
	private LerpAnimator AppointmentsLerp;
	private FadeObjectInOut AppointmentsFade;

	private static bool iconsInitiated = false;

	public Material Patient1;
	public Material Patient2;
	public Material Patient3;

	// Use this for initialization
	void Start () {

		calendarPatients = GameObject.Find("Calendar/Patients").GetComponentInChildren<Animator>();
		calendar = GameObject.Find("Calendar").GetComponentInChildren<Animator>();
		calendarHighlight = GameObject.Find("CalendarHighlights").GetComponentInChildren<Animator>();

		patientList = GameObject.Find("PatientList").GetComponentInChildren<Animator>();
		patientProfile = GameObject.Find("PatientProfile").GetComponentInChildren<Animator>();

		CalendarLerp = GameObject.Find("CalendarIcon").GetComponent<LerpAnimator>();

		calendarPatients.CrossFade ("CalendarIdle", .2F, -1);
		calendar.CrossFade ("CalendarIdle", .2F, -1);
		patientList.CrossFade ("PatientListIdle", .2F, -1);
		patientProfile.CrossFade ("PatientProfileIdle", .2F, -1);

		newsTreatments = GameObject.Find("News").GetComponentInChildren<Animator>();
		//productList = GameObject.Find("ProductList").GetComponentInChildren<Animator>();
		productProfile = GameObject.Find("ProductProfile").GetComponentInChildren<Animator>();

		AppointmentsFade = GameObject.Find ("NewAppointment").GetComponent<FadeObjectInOut> ();

		newsTreatments.CrossFade ("NewsIdle", .2F, -1);
		//productList.CrossFade ("ProductListIdle", .2F, -1);
		productProfile.CrossFade ("ProductProfileIdle", .2F, -1);

		NewsLerp = GameObject.Find("NewsIcon").GetComponent<LerpAnimator>();
		//AppointmentsLerp = GameObject.Find("NewAppointment").GetComponent<LerpAnimator>();

		GameObject.Find ("CalendarGlow").GetComponent<ParticleSystem>().enableEmission = false;

		// Hide date objects initially since they interfere with reticle 
		GameObject.Find ("CalendarHighlights").transform.localScale = new Vector3(0, 0, 0);

	}

	// Update is called once per frame
	void Update () {

	}

	// CALENDAR FUNCTIONS

	public void MaximizeCalendar(){

		// Bring calendar date highlight onto stage
		GameObject.Find ("CalendarHighlights").transform.localScale = new Vector3(5.565401F, 5.685142F, 6.138467F);

		if (newsOpen) {
			MinimizeNews ();
		}
			
		calendarOpen = true;

		StartCoroutine(ShowCalendarDate ());

		Debug.Log ("Showing Date: " + "Date_" + DateTime.Now.Day);

		//calendarPatients.CrossFade ("MaximizeCalendar", .2F, -1);
		calendar.CrossFade ("MaximizeCalendar",.2F,-1);

		//calendarHighlight.CrossFade ("CalendarHighlightScaleUp",.2F,-1);

		if(activeAnimation == "MaximizePatientList")
			patientList.CrossFade ("MinimizePatientList",.2F,-1);

		if(activeAnimation == "MaximizePatientProfile")
			patientProfile.CrossFade ("MinimizePatientProfile",.2F,-1);

		activeAnimation = "MaximizeCalendar";

		GameObject.Find ("CalendarGlow").GetComponent<ParticleSystem>().enableEmission = true;
		GameObject.Find ("NewsGlow").GetComponent<ParticleSystem>().enableEmission = false;

		if (!iconsInitiated) {
			MoveIconsToBottom ();
			iconsInitiated = true;
		}

		SoundSelect ();

	}


	public IEnumerator ShowCalendarDate()
	{           
		yield return new WaitForSeconds(.5F);
		GameObject.Find ("Date_" + DateTime.Now.Day).GetComponent<Renderer> ().enabled = true;	
	}

	public void HideCalendarDate()
	{           
		Debug.Log ("Hiding Calendar Highlight");
		GameObject.Find ("Date_" + DateTime.Now.Day).GetComponent<Renderer> ().enabled = false;	
	}


	public void MinimizeCalendar(){

		Debug.Log ("Closing Calendar, activeAnimation: "+activeAnimation);
		GameObject.Find ("Date_" + DateTime.Now.Day).GetComponent<Renderer> ().enabled = false;

		//calendarHighlight.CrossFade ("CalendarHighlightScaleDown",.2F,-1);

		if(activeAnimation == "MaximizePatientList")
			patientList.CrossFade ("MinimizePatientList",.2F,-1);

		if(activeAnimation == "MaximizeCalendar")
			calendarPatients.CrossFade ("MinimizeCalendar",.2F,-1);
		
		if(activeAnimation == "MaximizePatientProfile" || activeAnimation == "MinimizePatientProfile")
			patientProfile.CrossFade ("MinimizePatientProfile",.2F,-1);
		
		calendarOpen = false;

		activeAnimation = "MinimizeCalendar";
	}

	public void MaximizePatientList(){

		if (activeAnimation == "MaximizeCalendar") {
			patientList.CrossFade ("MaximizePatientList", .2F, -1);
			calendar.CrossFade ("MinimizeCalendar", .2F, -1);
			HideCalendarDate ();
			Debug.Log ("Minimizing calendar from MaximizePatientList()");

		}

		if (activeAnimation == "MaximizePatientProfile") {
			patientList.CrossFade ("MaximizePatientList", .2F, -1);
			patientProfile.CrossFade ("MinimizePatientProfile", .2F, -1);
			//StartCoroutine (ShowCalendarDate());
			Debug.Log ("Minimizing patient profile from MaximizePatientList()");
		}

		activeAnimation = "MaximizePatientList";

		SoundSelect ();

		Debug.Log ("activeAnimation: "+activeAnimation);

	}

	public void MaximizePatientProfile_1(){

		patientProfile.CrossFade ("MaximizePatientProfile", .2F, -1);

		patientList.CrossFade ("MinimizePatientList", .2F, -1);

		activeAnimation = "MaximizePatientProfile";

		GetComponent<Renderer> ().material = Patient1;

		Debug.Log ("activeAnimation: "+activeAnimation);

		SoundSelect ();

	}

	public void MaximizePatientProfile_2(){

		patientProfile.CrossFade ("MaximizePatientProfile", .2F, -1);

		patientList.CrossFade ("MinimizePatientList", .2F, -1);

		activeAnimation = "MaximizePatientProfile";

		GetComponent<Renderer> ().material = Patient2;

		Debug.Log ("activeAnimation: "+activeAnimation);

		SoundSelect ();

	}

	public void MaximizePatientProfile_3(){

		patientProfile.CrossFade ("MaximizePatientProfile", .2F, -1);

		patientList.CrossFade ("MinimizePatientList", .2F, -1);

		activeAnimation = "MaximizePatientProfile";

		GetComponent<Renderer> ().material = Patient3;

		Debug.Log ("activeAnimation: "+activeAnimation);

		SoundSelect ();

	}
	public void MinimizePatientProfile(){

		Debug.Log ("activeAnimation: "+activeAnimation);

		//if (activeAnimation == "MaximizePatientProfile") {
			patientProfile.CrossFade ("MinimizePatientProfile", .2F, -1);
			patientList.CrossFade ("MaximizePatientList", .2F, -1);
		//}

		activeAnimation = "MinimizePatientProfile";

		SoundSelect ();
	}

	// NEWS / TREATMENTS FUNCTIONS

	public void MaximizeNews(){

		newsOpen = true;

		// minimize calendar when maximizing news
		if (calendarOpen) {
			MinimizeCalendar ();
		}

		if(activeAnimation == "MaximizeProductProfile")
			patientList.CrossFade ("MinimizeProductProfile",.2F,-1);

		GameObject.Find ("CalendarGlow").GetComponent<ParticleSystem>().enableEmission = false;
		GameObject.Find ("NewsGlow").GetComponent<ParticleSystem>().enableEmission = true;

		newsTreatments.CrossFade ("NewsMaximize",.2F,-1);

		activeAnimation = "NewsMaximize";

		if (!iconsInitiated) {
			MoveIconsToBottom ();
			iconsInitiated = true;
		}

		SoundSelect ();

	}

	public void MinimizeNews(){

		newsOpen = false;

		newsTreatments.CrossFade ("NewsMinimize", .2F, -1);
		productProfile.CrossFade ("MinimizeProductProfile", .2F, -1);

		Debug.Log ("Closing News");

		newsOpen = false;

		activeAnimation = "NewsMinimize";
	}
		

	public void MaximizeProductProfile(){
		
		newsTreatments.CrossFade ("NewsMinimize", .2F, -1);

		productProfile.CrossFade ("MaximizeProductProfile", .2F, -1);

		Debug.Log ("Maximizing Product Profile");


		GameObject.Find ("CalendarGlow").GetComponent<ParticleSystem>().enableEmission = true;
		GameObject.Find ("NewsGlow").GetComponent<ParticleSystem>().enableEmission = false;

		activeAnimation = "MaximizeProductProfile";

		StartCoroutine(ShowAppointment ());
		//AppointmentsLerp.LerpAnimate (GameObject.Find ("NewAppointment"), GameObject.Find ("AppointmentPosition_2"));


		SoundSelect ();

	}

	public IEnumerator ShowAppointment()
	{           
		Debug.Log ("Show Appointment");
		yield return new WaitForSeconds(1F);

		AppointmentsFade.FadeIn ();

		//GameObject.Find ("NewAppointment").GetComponent<FadeObjectInOut> ().fadeInOnStart ();

		//AppointmentsLerp.LerpAnimate (GameObject.Find ("NewAppointment"), GameObject.Find ("AppointmentPosition_2"));
		//GameObject.Find ("NewAppointment").GetComponent<AudioSource>().Play();
	}

	public void MinimizeProductProfile(){

		//if (activeAnimation == "MaximizeProductProfile") {

			newsTreatments.CrossFade ("NewsMaximize", .2F, -1);

			productProfile.CrossFade ("MinimizeProductProfile", .2F, -1);

			Debug.Log ("Minimizing Product Profile");
			//NewsLerp.LerpAnimate (GameObject.Find ("News/Treatments"), GameObject.Find ("NewsPosition_2"));
		//}

		GameObject.Find ("CalendarGlow").GetComponent<ParticleSystem>().enableEmission = true;
		GameObject.Find ("NewsGlow").GetComponent<ParticleSystem>().enableEmission = false;

		// fade out appointment
		AppointmentsFade.FadeOut();

		activeAnimation = "MinimizeProductProfile";

		SoundSelect ();

	}


	// Sounds

	public void SoundRollover(){

		AudioSource audio = GameObject.Find("rollover").GetComponent<AudioSource>();
		audio.Play();

	}

	public void SoundSelect(){

		AudioSource audio = GameObject.Find("select").GetComponent<AudioSource>();
		audio.Play();

	}

	public void MoveIconsToBottom(){
		

		GameObject.Find ("MessageUnread").GetComponent<Renderer>().enabled = false;

		GameObject.Find ("MessageRead").GetComponent<Renderer>().enabled = true;

		Destroy (GameObject.Find ("NewMessage"));

		CalendarLerp.LerpAnimate (GameObject.Find ("CalendarIcon"), GameObject.Find ("CalendarBottom"));

		NewsLerp.LerpAnimate (GameObject.Find ("NewsIcon"), GameObject.Find("NewsBottom"));

	}
		
}

